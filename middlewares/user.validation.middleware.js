const { user } = require('../models/user');
const UserService = require('../services/userService');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const emailValid = /([A-Za-z0-9]{6,30}@gmail.com)/g;
    const phoneValid = /\+380[0-9]{9}$/g;
    const passwordValid = /[0-9a-zA-Z]{3,}/g;
    const nameValid = /([A-Z]{1}[a-z ]{1,}){1}$/g;
    let fieldCounter = 0;
    const newUser = {...user};
    if(req.body.id){
        req.error = true;
        req.message = 'Wrong field!'
        return next()
    }
    for(item in req.body){
        if(!newUser.hasOwnProperty(item)){
            req.error = true;
            req.message = 'Wrong field'
            return next()
        }
        else{
                newUser[item] = req.body[item];
        }
        fieldCounter++;
    }
    if(fieldCounter==0){
        req.error = true;
            req.message = 'Empty request'
            return next()
    }
    if(newUser.email?.match(emailValid)){//if email is correct
        if(UserService.search({email: newUser.email})){
            req.error = true;
            req.message = 'User with this email exists'
            return next()
        }
    }else{
        req.error = true;
        req.message = 'Wrong email'
        return next()
        
    }

    if(!newUser.password?.match(passwordValid)){//if password is not valid
        req.error = true;
            req.message = 'Bad password'
            return next()
    }

    if(newUser.phoneNumber?.match(phoneValid)){//if phone is valid
        if(UserService.search({phoneNumber: newUser.phoneNumber})){
            req.error = true;
            req.message ='Phone is taken by someone else'
            return next()
        }
    }
    else{
        req.error = true;
        req.message = 'Bad phone number'
        return next()
        
    }

    if(!newUser.firstName?.match(nameValid)){    //if first name is not valid
        req.error = true;
        req.message = 'Bad firstname'
        return next()
       
    }
    if(!newUser.lastName?.match(nameValid)){// if second name is not valid
        req.error = true;
        req.message = 'Bad lastname'
        return next()
    }
    req.data = {...newUser};
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const emailValid = /([A-Za-z0-9]{6,30}@gmail.com)/g;//same to adding user
    const phoneValid = /\+380[0-9]{9}$/g;
    const passwordValid = /[0-9a-zA-Z]{3,}/g;
    const nameValid = /([A-Z]{1}[a-z ]{1,}){1}$/g;
    const {id} = req.params;
    let user = UserService.getUserById(id);
    if(!user){//if wrong id
        req.error = true;
        req.status = 404;
        req.message = 'Wrong id'
        return next()
    }
    const enteredUpdateData = {}
    let fieldCounter = 0;
    if(req.body.id){
        req.error = true;
        req.message = 'Wrong field!'
        return next()
    }
    for(item in req.body){
        if(!user.hasOwnProperty(item)){
            req.error = true;
        req.message = 'Wrong field!'
        return next()
        }
        else{
                if(req.body[item]){
                    enteredUpdateData[item] = req.body[item]
                }
                else{
                    enteredUpdateData[item] = user[item]
                }
            
            
            fieldCounter++
        }
    }
    if(fieldCounter == 0){
        req.error = true;
        req.message = 'Empty request'
        return next()
    }
    if(enteredUpdateData.email){
        if(!enteredUpdateData.email?.match(emailValid)){
            req.error = true;
            req.message = 'bad email'
            return next()
           
        }else{
            let userFromDB = UserService.search({email: enteredUpdateData.email});
            if(userFromDB){//if user tries to have new email but it is taken
                req.error = true;
                req.message = 'bad email(is taken)'
                return next()
                
            } 
        }
    }

    if(enteredUpdateData.password){
        if(!enteredUpdateData.password?.match(passwordValid)){
            req.error = true;
            req.message =  'bad password'
            return next()
        }
    }

    if(enteredUpdateData.phoneNumber){
        if(!enteredUpdateData.phoneNumber?.match(phoneValid)){
            req.error = true;
            req.message =  'bad phone'
            return next()
            
        }
        else{
            let userFromDB = UserService.search({phoneNumber: enteredUpdateData.phoneNumber});
            if(userFromDB){
                req.error = true;
            req.message =  'bad phone(is taken)'
            return next()
                
            }
        }
    }

    if(enteredUpdateData.firstName){
        if(!enteredUpdateData.firstName?.match(nameValid)){
            req.error = true;
            req.message =  'bad firstname'
            return next()
           
        }
    }

    
    if(enteredUpdateData.lastName){
        if(!enteredUpdateData.lastName?.match(nameValid)){
            req.error = true;
            req.message =  'bad lastname'
            return next()
        }
    }

    enteredUpdateData.firstName = enteredUpdateData.firstName ? enteredUpdateData.firstName : user.firstName
    enteredUpdateData.lastName = enteredUpdateData.lastName ? enteredUpdateData.lastName : user.lastName
    enteredUpdateData.email = enteredUpdateData.email ? enteredUpdateData.email : user.email
    enteredUpdateData.phoneNumber = enteredUpdateData.phoneNumber ? enteredUpdateData.phoneNumber : user.phoneNumber
    enteredUpdateData.password = enteredUpdateData.password ? enteredUpdateData.password : user.password



    req.data = {...enteredUpdateData};
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;