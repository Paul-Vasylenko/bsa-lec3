const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const nameValid = /([A-Z]{1}[a-z ]{1,}){1,3}$/g;
    let fieldCounter = 0;

    const newFighter = {...fighter}
    if(req.body.id){
        req.error = true;
        req.message = 'Wrong field!'
        return next()
    }
    for(item in req.body){
        if(!newFighter.hasOwnProperty(item)){
            req.error = true;
            req.message = 'Wrong field'
            return next()
        }
        else{
            if(item == "name"){
                newFighter[item] = req.body[item];
            }
            else{
                    newFighter[item] = +req.body[item];
            }
        }
        fieldCounter++;
    }

    if(fieldCounter==0){
        req.error = true;
            req.message =  'Empty request'
            return next()
        
    }

    if(newFighter.name?.match(nameValid)){
        let fighterExist = FighterService.search({name: newFighter.name})
        if(fighterExist){
            req.error = true;
            req.message =  'Fighter with this name exists'
            return next()
            
        }
    }
    else{
        req.error = true;
            req.message =  'Bad name'
            return next()
       
    }

    if(!newFighter.health){
        newFighter.health=100;
    }
    else if(newFighter.health<80 || newFighter.health>120){
        req.error = true;
        req.message =  'bad health'
        return next()
      
    }

    if(!newFighter.defense || newFighter.defense<1 || newFighter.defense>10){
        req.error = true;
        req.message =  'bad defense'
        return next()
        
    }

    if(!newFighter.power || newFighter.power<1 || newFighter.power>100){
        req.error = true;
        req.message = 'bad power'
        return next()
    }

    req.data = newFighter;
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const nameValid = /([A-Z]{1}[a-z ]{1,}){1,3}$/g;
    const {id} = req.params;
    const fighterUpdate = FighterService.getFighterById(id);
    const updateData = {};

    if(!fighterUpdate){
        req.error = true;
        req.status = 404;
        req.message ='Wrong id'
        return next()
    }
    if(req.body.id){
        req.error = true;
        req.message = 'Wrong field!'
        return next()
    }
    let fieldCounter = 0;
    for(item in req.body){
        if(!fighterUpdate.hasOwnProperty(item)){
            req.error = true;
        req.message = 'Wrong field!'
        return next()
            
        }
        else{
                if(item=="name"){
                    updateData[item] = req.body[item]
                }
                else{
                    updateData[item] = +req.body[item]
                }
            
            fieldCounter++
        }
    }
    if(fieldCounter == 0){
        req.error = true;
        req.message =  'Empty request'
        return next()
       
    }
    if(req.body.name?.match(nameValid)){
        let fighterExist = FighterService.search({name: req.body.name});
        if(fighterExist){
            req.error = true;
            req.message =  'bad name(is taken)'
            return next()
           
        }
    }else{
        if(req.body.name){
            req.error = true;
            req.message = 'bad name'
            return next()
       
        }
    }

    if(req.body.health){
        if(req.body.health>120 || req.body.health<80){
            req.error = true;
            req.message = 'bad health'
            return next()
            
        }
    }

    if(req.body.power){
        if(req.body.power>100 || req.body.power<1){
            req.error = true;
            req.message = 'bad power'
            return next()
            
        }
    }

    if(req.body.defense){
        if(req.body.defense>10 || req.body.defense<1){
            req.error = true;
            req.message = 'bad defense'
            return next()
        }
    }

    fighterUpdate.name = updateData.name ? updateData.name : fighterUpdate.name;
    fighterUpdate.power = updateData.power ? updateData.power : fighterUpdate.power;
    fighterUpdate.health = updateData.health ? updateData.health : fighterUpdate.health;
    fighterUpdate.defense = updateData.defense? updateData.defense : fighterUpdate.defense;

    req.data = fighterUpdate
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;