const responseMiddleware = (req, res, next) => {
        // TODO: Implement middleware that returns result of the query
        if (req.error) {
            return res.status(req.status ? req.status : 400).json({
                error: true,
                message: req.message,
            });
        } else {
            if (req.data) {
                return res.status(200).json(req.data);
            } else {
                return res.status(200).json({
                    message: req.message
                });
            }
        }
        next();
}

exports.responseMiddleware = responseMiddleware