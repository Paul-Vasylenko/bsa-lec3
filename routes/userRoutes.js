const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { getUserById, deleteUserById } = require('../services/userService');

const router = Router();

// TODO: Implement route controllers for user

router.get('/',(req, res, next) => {
        const users = UserService.getAllUsers();
        if (!users) {
            req.error = true;
            req.status = 404;
            req.message = 'Users not found';
            return next();
        }
        req.data = users;
        req.message = 'Users found';
        next();
},responseMiddleware);

router.get('/:id',(req, res, next) => {
        const {id} = req.params;
        const user = UserService.getUserById(id);
        if (!user) {
            req.error = true;
            req.status = 404;
            req.message = 'User not found';
            return next();
        }
        req.message = 'User found';
        req.data = user;
        next();
},responseMiddleware);

router.post('/',createUserValid,(req, res, next) => {
        const user = req.data;
        if (!req.error) {
            UserService.createUser(user);
            req.message = 'User created';
        }
        next();
},responseMiddleware);

router.put('/:id',updateUserValid,(req, res, next) => {
        const {id} = req.params;
        const user = req.data;
        if (!req.error) {
            UserService.updateUserById(id, user);
            req.message = 'User updated';
        }
        next();
},responseMiddleware);

router.delete('/:id',(req, res, next) => {
        const {id} = req.params;
        const user = UserService.getUserById(id);
        if (!user) {
            req.error = true;
            req.status = 404;
            req.message = 'User not found';
            return next();
        }
        UserService.deleteUserById(id);
        req.data = user;
        next();
},responseMiddleware);

module.exports = router;