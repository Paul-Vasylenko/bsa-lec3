const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', (req,res,next) => {
    const fighters = FighterService.getAllFighters();
    if(!fighters){
        req.error = true;
            req.status = 404;
            req.message = 'Fighters not found';
            return next();
    }
        req.data = fighters;
        req.message = 'Fighters found';
        next();
},responseMiddleware)

router.get('/:id', (req,res,next) => {
    const {id} = req.params;
    const fighter = FighterService.getFighterById(id);
    if(!fighter) {
        req.error = true;
        req.status = 404;
        req.message = 'Fighter not found';
        return next();
    }
    req.message = 'fighter found';
    req.data = fighter;
    next();
},responseMiddleware)

router.post('/', createFighterValid, (req,res,next)=>{
    const fighter = req.data;
    if (!req.error) {
        FighterService.createFighter(fighter);
        req.message = 'User created';
    }
    next();
},responseMiddleware)

router.put('/:id', updateFighterValid, (req,res,next) => {
    const {id} = req.params;
    const fighter = req.data;
    if (!req.error) {
        FighterService.updateFighterById(id,fighter);
        req.message = 'User updated';
    }
    next()
},responseMiddleware)

router.delete('/:id', (req,res,next) => {
    const {id} = req.params;
    const fighter = FighterService.getFighterById(id);
    if (!fighter) {
        req.error = true;
        req.status = 404;
        req.message = 'fighter not found';
        return next();
    }
    FighterService.deleteFighterById(id);
    req.data = fighter;
    next();
},responseMiddleware)

module.exports = router;