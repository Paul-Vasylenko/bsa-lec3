const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getAllFighters(){
        const fighters = FighterRepository.getAll();
        if(!fighters){
            return null;
        }
        return fighters;
    }
    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    getFighterById(id){
        return this.search({id});
    }
    createFighter(fighter){
        return FighterRepository.create(fighter);
    }
    updateFighterById(id,fighter){
        FighterRepository.update(id,fighter);
    }
    deleteFighterById(id){
        FighterRepository.delete(id);
    }
}

module.exports = new FighterService();