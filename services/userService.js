const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    getAllUsers() {
        const users = UserRepository.getAll();
        if (users.length==0) {
            return null;
        }
        return users;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    
    getUserById(id){
        return this.search({id});
    }

    createUser(user){
        return UserRepository.create(user);
    }
    updateUserById(id, user){
        UserRepository.update(id,user);
    }
    deleteUserById(id){
        UserRepository.delete(id);
    }
}

module.exports = new UserService();